from datetime import datetime
from flask import render_template, session, redirect, url_for, flash, request, current_app
from flask_login import login_required, current_user
from .forms import FarmersUploadForm, FactoryForm, CentreForm, ProduceForm, FarmersForm, SeasonForm, ContractForm, RouteForm,VehicleForm, GraderForm, DriverForm, TripForm, CompleteTripForm
from ..models import Factory, CollectionCentre, Produce, Farmer, Season, Collection, FarmerContract, Route, Vehicle, AppUser, Role, Driver, Trip, TripPayment
from app import db
from werkzeug.utils import secure_filename
import os
import uuid
import re
import csv


from . import main

@main.route('/', methods=['GET', 'POST'])
@login_required
def index():
    # LECH CODE HERE
    farmer_count = Farmer.query.count()
    graders_count = AppUser.query.count()
    routes_count = Route.query.count()
    centre_count = CollectionCentre.query.count()
    return render_template('main/dashboard.html', centre_count=centre_count, farmers_count=farmer_count, graders_count=graders_count, routes_count=routes_count)


@main.route('/factories', methods=['GET', 'POST'])
@login_required
def factories():
    factories = Factory.query.all()
    return render_template('main/factories.html', factories=factories)


@main.route('/new-factory', methods=['GET', 'POST'])
@login_required
def new_factory():
    form = FactoryForm()
    if form.validate_on_submit():
        factory = Factory(code=form.code.data, name=form.name.data, creator=current_user)
        db.session.add(factory)
        db.session.commit()
        flash('The factory was added successfully.', 'success')
        return redirect(url_for('.factories'))
    return render_template('main/new_factory.html', form=form)


@main.route('/factory/<int:id>', methods=['GET', 'POST'])
@login_required
def edit_factory(id):
    factory = Factory.query.get_or_404(id)
    form = FactoryForm()
    form.code.data = factory.code
    form.name.data = factory.name
    if form.validate_on_submit():
        factory.code = request.form.get('code')
        factory.name = request.form.get('name')
        db.session.add(factory)
        db.session.commit()
        flash('The factory was updated successfully.', 'success')
        return redirect(url_for('.factories'))
    return render_template('main/new_factory.html', form=form)

@main.route('/seasons', methods=['GET', 'POST'])
@login_required
def seasons():
    seasons = Season.query.all()
    return render_template('main/seasons.html', seasons=seasons)


@main.route('/new-season', methods=['GET', 'POST'])
@login_required
def new_season():
    form = SeasonForm()
    if form.validate_on_submit():
        season = Season(code=form.code.data, name=form.name.data, creator=current_user)
        db.session.add(season)
        db.session.commit()
        flash('The season was added successfully.', 'success')
        return redirect(url_for('.seasons'))
    return render_template('main/new_season.html', form=form)
    


@main.route('/produce', methods=['GET', 'POST'])
@login_required
def produce():
    produce = Produce.query.all()
    return render_template('main/produce.html', produce=produce)


@main.route('/new-produce', methods=['GET', 'POST'])
@login_required
def new_produce():
    form = ProduceForm()
    if form.validate_on_submit():
        produce = Produce(code=form.code.data, name=form.name.data, creator=current_user, cess=form.cess.data)
        db.session.add(produce)
        db.session.commit()
        flash('The Produce was added successfully.', 'success')
        return redirect(url_for('.produce'))
    return render_template('main/new_produce.html', form=form)


@main.route('/produce/<int:id>', methods=['GET', 'POST'])
@login_required
def edit_produce(id):
    produce = Produce.query.get_or_404(id)
    form = ProduceForm()
    form.code.data = produce.code
    form.name.data = produce.name
    form.cess.data = produce.cess
    if form.validate_on_submit():
        produce.code = request.form.get('code')
        produce.name = request.form.get('name')
        produce.cess = request.form.get('cess')
        db.session.add(produce)
        db.session.commit()
        flash('The produce was updated successfully.', 'success')
        return redirect(url_for('.produce'))
    return render_template('main/new_produce.html', form=form)



@main.route('/centres', methods=['GET', 'POST'])
@login_required
def centres():
    centres = CollectionCentre.query.all()
    return render_template('main/centres.html', centres=centres)


@main.route('/routes', methods=['GET', 'POST'])
@login_required
def routes():
    routes = Route.query.all()
    return render_template('main/routes.html', routes=routes)


@main.route('/new-route', methods=['GET', 'POST'])
@login_required
def new_route():
    form = RouteForm()
    if form.validate_on_submit():
        route = Route(code=form.code.data, name=form.name.data, creator=current_user)
        db.session.add(route)
        db.session.commit()
        flash('Route was added successfully.', 'success')
        return redirect(url_for('.routes'))
    return render_template('main/new_route.html', form=form)


@main.route('/route/<int:id>', methods=['GET', 'POST'])
@login_required
def edit_route(id):
    route = Route.query.get_or_404(id)
    form = RouteForm()
    form.code.data = route.code
    form.name.data = route.name
    if form.validate_on_submit():
        route.code = request.form.get('code')
        route.name = request.form.get('name')
        db.session.add(route)
        db.session.commit()
        flash('The route was updated successfully.', 'success')
        return redirect(url_for('.routes'))
    return render_template('main/new_route.html', form=form)



@main.route('/new-vehicle', methods=['GET', 'POST'])
@login_required
def new_vehicle():
    form = VehicleForm()
    if form.validate_on_submit():
        vehicle = Vehicle(reg_no=form.reg_no.data, make=form.make.data, ownership=form.ownership.data, color=form.color.data, body=form.body.data, creator=current_user)
        db.session.add(vehicle)
        db.session.commit()
        flash('Motor Vehicle added successfully.', 'success')
        return redirect(url_for('.vehicles'))
    return render_template('main/new_vehicle.html', form=form)


@main.route('/vehicle/<int:id>', methods=['GET', 'POST'])
@login_required
def edit_vehicle(id):
    vehicle = Vehicle.query.get_or_404(id)
    form = VehicleForm()
    form.reg_no.data = vehicle.reg_no
    form.make.data = vehicle.make
    form.ownership.data = vehicle.ownership
    form.color.data = vehicle.color
    form.body.data = vehicle.body
    if form.validate_on_submit():
        vehicle.reg_no = request.form.get('reg_no')
        vehicle.make = request.form.get('make')
        vehicle.ownership = request.form.get('ownership')
        vehicle.color = request.form.get('color')
        vehicle.body = request.form.get('body')
        db.session.add(vehicle)
        db.session.commit()
        flash('The vehicle was updated successfully.', 'success')
        return redirect(url_for('.vehicles'))
    return render_template('main/new_vehicle.html', form=form)


@main.route('/vehicles', methods=['GET', 'POST'])
@login_required
def vehicles():
    vehicles = Vehicle.query.all()
    return render_template('main/vehicles.html', vehicles=vehicles)


@main.route('/graders', methods=['GET', 'POST'])
@login_required
def graders():
    objects = AppUser.query.all()
    return render_template('main/graders.html', objects=objects)


@main.route('/new_grader', methods=['GET', 'POST'])
@login_required
def new_grader():
    form = GraderForm()
    form.role.choices = [(row.id, row.name) for row in Role.query.all()]
    if form.validate_on_submit():
        grader = AppUser(
                first_name=form.first_name.data, 
                last_name=form.last_name.data, 
                phone_number=form.phone_number.data, 
                username=form.app_username.data, 
                app_password=form.app_password.data, 
                creator=current_user,
                role_id = form.role.data
            )
        db.session.add(grader)
        db.session.commit()
        flash('Grader added successfully.', 'success')
        return redirect(url_for('.graders'))
    return render_template('main/new_grader.html', form=form)



@main.route('/grader/<int:id>', methods=['GET', 'POST'])
@login_required
def edit_grader(id):
    grader = AppUser.query.get_or_404(id)
    form = GraderForm(role=grader.role_id)
    form.first_name.data = grader.first_name
    form.last_name.data = grader.last_name
    form.phone_number.data = grader.phone_number
    form.app_username.data = grader.username
    form.app_password.data = grader.app_password
    form.role.choices = [(row.id, row.name) for row in Role.query.all()]
    if form.validate_on_submit():
        grader.first_name = request.form.get('first_name')
        grader.last_name = request.form.get('last_name')
        grader.phone_number = request.form.get('phone_number')
        grader.username = request.form.get('app_username')
        grader.app_password = request.form.get('app_password')
        grader.role_id = request.form.get('role')
        db.session.add(grader)
        db.session.commit()
        flash('The grader was updated successfully.', 'success')
        return redirect(url_for('.graders'))
    return render_template('main/new_grader.html', form=form)


@main.route('/grader/<int:id>/trips', methods=['GET', 'POST'])
@login_required
def grader_trips(id):
    grader = AppUser.query.get_or_404(id)
    objects = Trip.query.filter_by(grader_id=grader.id)
    return render_template('main/grader_trips.html', objects=objects, grader=grader)

@main.route('/driver/<int:id>/trips', methods=['GET', 'POST'])
@login_required
def driver_trips(id):
    driver = Driver.query.get_or_404(id)
    objects = Trip.query.filter_by(driver_id=driver.id) # SELECT * FROM TRIPS WHERE driver_id = 121
    return render_template('main/driver_trips.html', objects=objects, driver=driver)


@main.route('/trips', methods=['GET', 'POST'])
@login_required
def trips():
    objects = Trip.query.all()
    return render_template('main/trips.html', objects=objects)


@main.route('/new_trip', methods=['GET', 'POST'])
@login_required
def new_trip():
    form = TripForm()
    form.route.choices = [(row.id, row.name) for row in Route.query.all()]
    form.grader.choices = [(row.id, row.fullname) for row in AppUser.query.filter_by(role_id=2)]
    form.driver.choices = [(row.id, row.fullname) for row in Driver.query.all()]
    form.vehicle.choices = [(row.id, row.reg_no) for row in Vehicle.query.all()]
    if form.validate_on_submit():


        status = db.Column(db.String(20), unique=True, index=True)
        total_weight_collected = db.Column(db.Float)
        total_weight_received = db.Column(db.Float)
        trip_date = datetime.strptime(form.trip_date.data,"%m/%d/%Y %I:%M %p")
        trip = Trip(
                draft_date = trip_date,
                route_id=form.route.data, 
                driver_id=form.driver.data, 
                vehicle_id=form.vehicle.data, 
                grader_id=form.grader.data, 
                user_id=current_user.id,
                status="OPEN"
            )
        db.session.add(trip)
        db.session.commit()
        flash('Trip scheduled successfully.', 'success')
        return redirect(url_for('.trips'))
    return render_template('main/new_trip.html', form=form)




@main.route('/drivers')
@login_required
def drivers():
    drivers = Driver.query.all()
    return render_template('main/drivers.html', objects=drivers)


@main.route('/new_driver', methods=['GET', 'POST'])
@login_required
def new_driver():
    form = DriverForm()
    if form.validate_on_submit():
        driver = Driver(
                first_name=form.first_name.data, 
                last_name=form.last_name.data, 
                phone_number=form.phone_number.data,
                creator=current_user
            )
        db.session.add(driver)
        db.session.commit()
        flash('Driver added successfully.', 'success')
        return redirect(url_for('.drivers'))
    return render_template('main/new_driver.html', form=form)


@main.route('/driver/<int:id>', methods=['GET', 'POST'])
@login_required
def edit_driver(id):
    driver = Driver.query.get_or_404(id)
    form = DriverForm()
    form.first_name.data = driver.first_name
    form.last_name.data = driver.last_name
    form.phone_number.data = driver.phone_number
    if form.validate_on_submit():
        driver.first_name = request.form.get('first_name')
        driver.last_name = request.form.get('last_name')
        driver.phone_number = request.form.get('phone_number')
        db.session.add(driver)
        db.session.commit()
        flash('The driver was updated successfully.', 'success')
        return redirect(url_for('.drivers'))
    return render_template('main/new_driver.html', form=form)




@main.route('/stations', methods=['GET', 'POST'])
@login_required
def stations():
    stations = CollectionCentre.query.all()
    return render_template('main/stations.html', stations=stations)



@main.route('/new-centre', methods=['GET', 'POST'])
@login_required
def new_centre():
    form = CentreForm()
    form.route.choices = [(row.id, row.name) for row in Route.query.all()]
    if form.validate_on_submit():
        centre = CollectionCentre(code=form.code.data, name=form.name.data, collection_centres_creator=current_user, route_id=form.route.data)
        db.session.add(centre)
        db.session.commit()
        flash('The centre was added successfully.', 'success')
        return redirect(url_for('.centres'))
    return render_template('main/new_centre.html', form=form)

@main.route('/centre/<int:id>/farmers', methods=['GET', 'POST'])
@login_required
def centre_farmers(id):
    centre = CollectionCentre.query.get_or_404(id)
    farmers = Farmer.query.filter_by(centre_id=centre.id)
    return render_template('main/centre_farmers.html', farmers=farmers, centre=centre)




@main.route('/centre/<int:id>', methods=['GET', 'POST'])
@login_required
def edit_centre(id):
    centre = CollectionCentre.query.get_or_404(id)
    form = CentreForm(route=centre.route_id)
    form.code.data = centre.code
    form.name.data = centre.name
    form.route.choices = [(row.id, row.name) for row in Route.query.all()]
    if form.validate_on_submit():
        centre.code = request.form.get('code')
        centre.name = request.form.get('name')
        centre.route_id = request.form.get('route')
        db.session.add(centre)
        db.session.commit()
        flash('The centre was updated successfully.', 'success')
        return redirect(url_for('.centres'))
    return render_template('main/new_centre.html', form=form)



@main.route('/farmers')
@login_required
def farmers():
    farmers = Farmer.query.all()
    return render_template('main/farmers.html', farmers=farmers)


@main.route('/farmer/<int:id>')
@login_required
def farmer(id):
    farmer = Farmer.query.get_or_404(id)
    contracts = FarmerContract.query.filter_by(farmer_id=farmer.id)
    collections = Collection.query.filter_by(farmer_id=farmer.id)
    return render_template('main/farmer.html', farmer=farmer, collections=collections, contracts=contracts)



@main.route('/farmer/<int:id>/new-contract', methods=['GET', 'POST'])
@login_required
def new_contract(id):
    farmer = Farmer.query.get_or_404(id)
    form = ContractForm()
    form.factory.choices = [(row.id, row.name) for row in Factory.query.all()]
    form.produce.choices = [(row.id, row.name) for row in Produce.query.all()]
    if form.validate_on_submit():
        # Deactivate current contract
        contract_no = farmer.supplier_no
        cuurent_contract = FarmerContract.query.filter(FarmerContract.farmer_id == farmer.id, FarmerContract.active == True).first()
        if cuurent_contract:
            contract_no = farmer.supplier_no + "-" + str(cuurent_contract.id)
            cuurent_contract.active = False
            db.session.add(cuurent_contract)
        contract = FarmerContract(
            produce_id=form.produce.data, 
            farmer_id=farmer.id, 
            contract_no=contract_no, 
            factory_id=form.factory.data, 
            price=form.price.data,
            payment_term=form.payment_term.data
        )
        db.session.add(contract)
        db.session.commit()
        flash('Contract added successfully.', 'success')
        return redirect(url_for('.farmer', id=farmer.id))
    return render_template('main/new_contract.html', farmer=farmer, form=form)



@main.route('/trip/<int:id>/collections', methods=['GET', 'POST'])
@login_required
def trip_collections(id):
    trip = Trip.query.get_or_404(id)
    variance = (trip.total_weight_received) - (trip.total_weight_collected)
    collections = Collection.query.filter(Collection.trip_id == trip.id).all()
    payments = TripPayment.query.filter_by(trip_id=trip.id)
    return render_template('main/trip_collections.html', variance=variance, collections=collections, trip=trip, payments=payments)



@main.route('/trip/<int:trip_id>/collection/<int:collection_id>', methods=['GET', 'POST'])
@login_required
def invalidate_collection(trip_id, collection_id):
    trip = Trip.query.get_or_404(trip_id)
    if trip.status == "CLOSED":
        flash("TRIP ALREADY CLOSED. YOU CAN NOT INVALIDATE COLLECTIONS FOR A CLOSED TRIP", "error")
        return redirect(url_for('.edit_trip', id=trip.id))
    collection = Collection.query.get_or_404(collection_id)
    collection.invalidated = True
    db.session.add(trip)
    db.session.commit()
    flash('Collection Invalidated Successfully.', 'success')
    return redirect(url_for('.edit_trip', id=trip.id))

@main.route('/trip/<int:trip_id>/collection/validate/<int:collection_id>', methods=['GET', 'POST'])
@login_required
def validate_collection(trip_id, collection_id):
    trip = Trip.query.get_or_404(trip_id)
    if trip.status == "CLOSED":
        flash("TRIP ALREADY CLOSED. YOU CAN NOT VALIDATE COLLECTIONS FOR A CLOSED TRIP", "error")
        return redirect(url_for('.edit_trip', id=trip.id))
    collection = Collection.query.get_or_404(collection_id)
    collection.invalidated = False
    db.session.add(trip)
    db.session.commit()
    flash('Collection Validated Successfully.', 'success')
    return redirect(url_for('.edit_trip', id=trip.id))


@main.route('/trip/<int:id>', methods=['GET', 'POST'])
@login_required
def edit_trip(id):
    variance = 0
    trip = Trip.query.get_or_404(id)
    variance = (trip.total_weight_received) - (trip.total_weight_collected)

    collections = Collection.query.filter(Collection.trip_id == trip.id).all()
    invalidated_collections = Collection.query.filter(Collection.invalidated == True, Collection.trip_id == trip.id).all()

    payments = TripPayment.query.filter_by(trip_id=trip.id)
    form = CompleteTripForm()
    if request.method == 'POST':
        if trip.status == "CLOSED":
            flash("TRIP ALREADY CLOSED", "error")
            return redirect(url_for('.edit_trip', id=trip.id))
        if form.validate_on_submit():
            trip.total_weight_received = form.total_weight_received.data
            trip.status = "CLOSED"
            db.session.add(trip)
            db.session.commit()
            flash('Trip Completed successfully.', 'success')
            return redirect(url_for('.edit_trip', id=trip.id))
    return render_template('main/edit_trip.html', variance=variance, collections=collections, invalidated_collections=invalidated_collections, form=form, trip=trip, payments=payments)


def get_farmer_contract(farmer_id):
    return FarmerContract.query.filter(FarmerContract.farmer_id == farmer_id, FarmerContract.active == True).first()

def get_gross_payment(rate, weight):
    return rate * weight

def get_net_payment(gross, kdb):
    return gross-kdb

def get_kdb_amount(weight):
    produce = Produce.query.first()
    rate = produce.cess
    return weight*rate


@main.route('/trip/<int:id>/payments', methods=['GET', 'POST'])
@login_required
def generate_trip_payments(id):
    trip = Trip.query.get_or_404(id)
    if trip.status == "OPEN":
        flash("TRIP STILL OPEN. CLOSE THE TRIP FIRST BEFORE GENERATING PAYMENTS", "error")
        return redirect(url_for('.edit_trip', id=trip.id))
    collections = Collection.query.filter(Collection.invalidated.isnot(True), Collection.payment_generated.isnot(True), Collection.trip_id == trip.id)
    
    for col in collections:
        contract = get_farmer_contract(col.farmer_id)
        farmer = Farmer.query.filter_by(id=col.farmer_id).first()
        if not contract:
            flash("FARMER {} HAS NO CONTRACT. SETUP HIS CONTRACT FIRST BEFORE GENERATING PAYMENTS".format(farmer.fullname), "error")
            return redirect(url_for('.edit_trip', id=trip.id))
        payment = TripPayment(
            status = "NOT PAID",
            rate = contract.price,
            kdb = get_kdb_amount(col.produce_weight),
            weight = col.produce_weight,
            gross = get_gross_payment(contract.price, col.produce_weight),
            net = get_net_payment(get_gross_payment(contract.price, col.produce_weight), get_kdb_amount(col.produce_weight)),
            trip_id = trip.id,
            contract_id = contract.id,
            farmer_id = col.farmer_id,
            delivery_date = col.collection_date
            )
        col.payment_generated = True
        db.session.add(col)
        db.session.add(payment)
    db.session.commit()
    flash('Payments Generated successfully.', 'success')
    return redirect(url_for('.edit_trip', id=trip.id))

@main.route('/collections')
@login_required
def collections():
    collections = Collection.query.all()
    return render_template('main/collections.html', collections=collections)


@main.route('/new-farmer', methods=['GET', 'POST'])
@login_required
def new_farmer():
    form = FarmersForm()
    upload_form = FarmersUploadForm()
    form.centre.choices = [(row.id, row.name) for row in CollectionCentre.query.all()]
    if form.validate_on_submit():
        farmer = Farmer(
            supplier_no=form.supplier_no.data, 
            first_name=form.first_name.data, 
            last_name=form.last_name.data, 
            creator=current_user, 
            centre_id=form.centre.data,
            id_number = form.id_number.data,
            phone_number = form.phone_number.data
        )
        db.session.add(farmer)
        db.session.commit()
        flash('The farmer was added successfully. Create a new contract for the farmer now', 'success')
        return redirect(url_for('.farmers'))
    return render_template('main/new_farmer.html', form=form, upload_form=upload_form)



@main.route('/farmer/edit/<int:id>/deactivte', methods=['GET', 'POST'])
@login_required
def deactivate_farmer(id):
    farmer = Farmer.query.get_or_404(id)
    farmer.status = "INACTIVE"
    db.session.add(farmer)
    db.session.commit()
    return redirect(url_for('.farmer', id=farmer.id))

@main.route('/farmer/edit/<int:id>/activate', methods=['GET', 'POST'])
@login_required
def activate_farmer(id):
    farmer = Farmer.query.get_or_404(id)
    farmer.status = "ACTIVE"
    db.session.add(farmer)
    db.session.commit()
    return redirect(url_for('.farmer', id=farmer.id))

@main.route('/farmer/edit/<int:id>', methods=['GET', 'POST'])
@login_required
def edit_farmer(id):
    farmer = Farmer.query.get_or_404(id)
    upload_form = FarmersUploadForm()
    form = FarmersForm(centre=farmer.centre_id, obj=farmer)
    form.centre.choices = [(row.id, row.name) for row in CollectionCentre.query.all()]
    if form.validate_on_submit():
        farmer.route_id = request.form.get('route')
        farmer.supplier_no=request.form.get('supplier_no')
        farmer.first_name=request.form.get('first_name')
        farmer.last_name=form.last_name.data
        farmer.centre_id=request.form.get('centre')
        farmer.id_number = request.form.get('id_number')
        farmer.phone_number = request.form.get('phone_number')
        db.session.add(farmer)
        db.session.commit()
        flash('The farmer was updated successfully.', 'success')
        return redirect(url_for('.farmer', id=farmer.id))
    return render_template('main/new_farmer.html', form=form, upload_form=upload_form)


@main.route('/farmer-upload', methods=['GET', 'POST'])
def farmer_upload():
    form = FarmersUploadForm()
    if form.validate_on_submit():
        filename = secure_filename(form.farmers_csv.data.filename)
        extension = filename.rsplit('.', 1)[1]
        farmers_csv = str(uuid.uuid4()) + '.' + extension
        form.farmers_csv.data.save(os.path.join(current_app.config['UPLOAD_FOLDER'], farmers_csv))
        flash('The farmers were Staged successfully. After previewing the uploaded data you can go ahead and save it to database', 'success')
        return redirect(url_for('main.preview_import', file=farmers_csv.rsplit('.', 1)[0]))
    flash('Upload was not successful. Please check', 'error')
    return redirect(url_for('main.new_farmer'))


def read_csv_upload(file):
    data = []
    with open(file, newline='') as csvfile:
        spamreader = csv.reader(csvfile, delimiter=',')
        next(csvfile)
        for row in spamreader:
            member = {}
            member['supplier_no'] = row[0].strip()
            member['first_name'] = row[1].strip()
            member['last_name'] = row[2].strip()
            member['id_number'] = row[3].strip()
            member['phone_number'] = row[4].strip()
            member['centre'] = row[5].strip()
            data.append(member)
    return data

@main.route('/preview-import/<file>')
def preview_import(file):
    data = read_csv_upload(os.path.join(current_app.config['UPLOAD_FOLDER'],file+".csv"))  
    return render_template('main/preview_import.html', data=data, csv_file=file)

def get_farmer_centre(centre):
    return CollectionCentre.query.filter(CollectionCentre.name == centre).first()

@main.route('/save-upload/<file>')
def save_upload(file):
    farmers = read_csv_upload(os.path.join(current_app.config['UPLOAD_FOLDER'],file+".csv"))
    for farmer in farmers:
        centre = get_farmer_centre(farmer['centre'])
        farmer_obj = Farmer(
            supplier_no=farmer['supplier_no'], 
            first_name=farmer['first_name'], 
            last_name=farmer['last_name'], 
            creator=current_user, 
            centre_id= centre.id,
            id_number = farmer['id_number'],
            phone_number = farmer['phone_number']
        )
        db.session.add(farmer_obj)
    db.session.commit()
    flash('The farmers upload have saved successfully. Create a new contracts for the farmers now', 'success')
    return redirect(url_for('.farmers'))